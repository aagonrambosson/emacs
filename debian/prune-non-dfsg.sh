#!/bin/sh

xargs rm -rf < debian/non-dfsg-files
find . -type d -not -path '\./.git/*' -empty -delete

